# Somtijds Exchange Admin Improvements

This plugin speeds up story / page editing for the Tandem website

## Table Of Contents

* [Installation](#installation)
* [Usage](#usage)
* [Crafted by Inpsyde](#crafted-by-inpsyde)
* [License](#license)
* [Contributing](#contributing)

## Installation

The best way to use this package is through Composer:

```BASH
$ composer require somtijds/exchange-admin-improvements
```

## Usage

`// Todo`

## License

Copyright (c) 2018 Willem Prins, Somtijds

Good news, this plugin is free for everyone! Since it's released under the [MIT License](LICENSE) you can use it free of charge on your personal or commercial website.

## Contributing

All feedback / bug reports / pull requests are welcome.