<?php

namespace Somtijds\Exchange\AdminImprovements;

class ScriptsRemover
{
    /**
     *
     */
    public static function register() {
        add_action(
            'admin_enqueue_scripts',
            [self::class,'deregister_admin_scripts']
        );
        add_action(
            'wp_enqueue_scripts',
            [self::class,'deregister_core_scripts']
        );
    }

    /**
     *
     */
    public static function deregister_admin_scripts() {
        $handles = [
            //'leaflet_map_construct'     => '',
            'gform_tooltip_init'        => '',
            'wppusher-js'               => '',
            'font_awesome'              => '',
            'cookie-bar-js'             => '',
            //'uber-media-js'             => '',
            'somtijds-strippenkaart-js' => '',
            'gadwp-backend-ui'          => '',
        ];
        self::deregister( $handles );
    }

    /**
     *
     */
    public static function deregister_core_scripts() {
        $handles = [];

        self::deregister( $handles );

    }

    /**
     * Deregister scripts by handle
     *
     * @param $handles
     */
    public static function deregister( $handles ) {

        if (
            empty( $handles )
            || ! is_admin()
            || ! function_exists( 'get_current_screen' ) ) {
            return;
        }

        $screen = \get_current_screen();

        if ( ! $screen instanceof \WP_Screen ) {
            return;
        }

        if ( in_array( $screen->post_type, ['page','story'] ) ) {
            foreach ($handles as $handle => $priority) {
                \wp_deregister_script($handle);
            }
        }
    }


}