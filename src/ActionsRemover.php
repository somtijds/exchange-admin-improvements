<?php
/**
 * Created by PhpStorm.
 * User: willem
 * Date: 25.11.18
 * Time: 19:52
 */

namespace Somtijds\Exchange\AdminImprovements;


class ActionsRemover
{
    /**
     *
     */
    public static function register() {
        add_action(
            'current_screen',
            [self::class,'remove_actions']
        );
    }

    public static function remove_actions() {
        if (
            ! is_admin()
            || ! function_exists( 'get_current_screen' ) ) {
            return;
        }

        $screen = \get_current_screen();

        if ( $screen instanceof \WP_Screen && in_array( $screen->post_type, ['page','story'] )) {
            remove_action('wp_enqueue_media','wpuxss_eml_enqueue_media');
        }
    }
}