<?php # -*- coding: utf-8 -*-

/*
 * Plugin Name:       Exchange Admin Improvements
 * Description:       This plugin ensures that the editing experience on the Tandem Website is improved.
 * Author:            Willem Prins | Somtijds
 * Version:           0.0.1
 * Text Domain:       'exchange-admin-improvements'
 * Domain Path:       languages
 * License:           MIT
 */

namespace Somtijds\Exchange\AdminImprovements;

function errorNotice(string $message)
{
    foreach (['admin_notices', 'network_admin_notices'] as $hook) {
        add_action(
            $hook,
            function () use ($message) {
                $class = 'notice notice-error';
                printf(
                    '<div class="%1$s"><p>%2$s</p></div>',
                    esc_attr($class),
                    wp_kses_post($message)
                );
            }
        );
    }
}

/**
 * Handle any exception that might occur during plugin setup
 *
 * @param \Throwable $throwable The Exception
 *
 * @return void
 */
function handleException(\Throwable $throwable)
{
    do_action('somtijds.exchange.critical', $throwable);

    errorNotice(
        sprintf(
            '<strong>Error:</strong> %s <br><pre>%s</pre>',
            $throwable->getMessage(),
            $throwable->getTraceAsString()
        )
    );
}

/**
 * Initialize all the plugin things.
 *
 * @throws \Throwable
 */
function initialize()
{
    try {
        if (is_readable(__DIR__.'/vendor/autoload.php')) {
            /** @noinspection PhpIncludeInspection */
            require_once __DIR__.'/vendor/autoload.php';
        }

        ScriptsRemover::register();
        ActionsRemover::register();

    } catch (\Throwable $throwable) {
        handleException($throwable);
    }
}

add_action('plugins_loaded', __NAMESPACE__.'\\initialize', PHP_INT_MAX);